public class Book {
    public int BookId{get; set;}
    public string Title{get; set;}
    public string Genre{get; set;}
    // public int AuthorId{get; set;}
    public Author Author{get; set;}

    public Book(string title, string genre, Author author) {
        Title = title;
        Genre = genre;
        Author = author;
    }
}